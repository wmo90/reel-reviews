import os
from psycopg_pool import ConnectionPool

# pool = ConnectionPool(
#     conninfo=os.environ.get(
#         "postgresql://example_user:password@mar-15-et-database-service.default.svc.cluster.local/example_db"
#     )
# )

# x = "postgresql://example_user:password@mar-15-et-database-service.default.svc.cluster.local/example_db"

# pool = ConnectionPool(conninfo=x)

pool = ConnectionPool(conninfo=os.environ.get("DATABASE_URL"))
