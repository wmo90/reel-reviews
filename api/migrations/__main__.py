async def migrate():
    from . import down, up, LATEST, ZERO

    import os
    import sys

    # x = "postgresql://example_user:password@mar-15-et-database-service.default.svc.cluster.local/example_db"
    # db_url = x
    db_url = os.environ.get("DATABASE_URL")
    if db_url is None:
        print("Error: DATABASE_URL environment variable not set.")
        sys.exit(1)

    if len(sys.argv) < 2:
        print("Command: up|down [amount]")
        exit(1)
    direction = sys.argv[1]
    amount = sys.argv[2] if len(sys.argv) > 2 else None
    if direction == "up":
        if amount is None:
            amount = LATEST
        else:
            try:
                amount = int(amount)
            except ValueError:
                print(f"Unknown amount {amount}")
        await up(db_url, to=amount)
    elif direction == "down":
        if amount is None:
            amount = 1
        elif amount == "zero":
            amount = ZERO
        else:
            try:
                amount = int(amount)
            except ValueError:
                print(f"Unknown amount {amount}")
        await down(db_url, to=amount)


if __name__ == "__main__":
    from asyncio import run

    run(migrate())
