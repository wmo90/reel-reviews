import { useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";
import TokenContext from "./TokenContext";

function Logout() {
  const [Hidelogin, setHidelogin] = useContext(TokenContext);
  const navigate = useNavigate();
  // const FastAPI_URL = process.env.REACT_APP_FastAPI_URL;

  useEffect(() => {
    const handleLogout = async () => {
      const url = `https://mar-15-et-api.mod3projects.com/token`;
      const fetchConfig = {
        method: "DELETE",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setHidelogin(true);
        navigate("/");
      }
    };
    handleLogout();
  }, [setHidelogin, navigate, Hidelogin]);

  return null;
}

export default Logout;
